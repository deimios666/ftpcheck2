<?php

ini_set("log_errors", "On");
ini_set("error_log", "error.log");

function sanitize( $input ) {
    return preg_replace('/[^a-zA-Z0-9\.]/',"_",$input);
}

function deltree($path) {

    if (is_dir($path)) {
        if (version_compare(PHP_VERSION, '5.0.0') < 0) {
            $entries = array();
            if ($handle = opendir($path)) {
                while (false !== ($file = readdir($handle)))
                    $entries[] = $file;

                closedir($handle);
            }
        } else {
            $entries = scandir($path);
            if ($entries === false)
                $entries = array(); // just in case scandir fail...
        }

        foreach ($entries as $entry) {
            if ($entry != '.' && $entry != '..') {
                deltree($path . '/' . $entry);
            }
        }

        return rmdir($path);
    } else {
        return unlink($path);
    }
}

class ftp_util {

    protected $ftp_handle;
    protected $login_result;
    protected $tmpdir;

    function ftp_util($server, $user, $pass, $root) {
        $this->ftp_handle = ftp_connect($server);
        $this->login_result = ftp_login($this->ftp_handle, $user, $pass);
        if ((!$this->ftp_handle) || (!$this->login_result)) {
            exit(0);
        }
        ftp_raw($this->ftp_handle, "OPTS UTF8 ON");
        ftp_pasv($this->ftp_handle, true);
        $this->tmpdir = "/tmp/" . md5(time()) . "/";
        //echo "Mking tmpdir: ". $this->tmpdir."\n";
        mkdir($this->tmpdir);
        if (!file_exists($this->tmpdir)) {
            die("Couldn't create tmp dir");
        }
        $this->cd($root);
    }

    function cd($dir) {
        $result = ftp_chdir($this->ftp_handle, $dir);
    }

    function lst() {
        $lista = ftp_nlist($this->ftp_handle, ".");
        //print_r($lista);
        return $lista;
    }

    function isdir($file) {
        
        //return is_file($this->ftp_handle, $file);
        
        if (ftp_size($this->ftp_handle, $file) == -1) {
            //echo "$file is directory\n";
            return true;
        } else {
            //echo "$file is NOT a directory\n";
            return false;
        }
    }
    

    function get_file($file, $target) {
        if (ftp_size($this->ftp_handle, $file) > 8428800) {
            echo "$file too large!";
            //$this->cleanup();
            //$this->disconnect();
            //die("File too large!!!");
        } else {
            //echo "Getting $target -> $file ";
            ftp_get($this->ftp_handle, "$target", "$file", FTP_BINARY);
        }
    }

    function get_file_r($file, $tmpadd) {
        if ($this->isdir($file)) {
            @mkdir($this->tmpdir . $tmpadd . $file, 0770, true);
            $this->cd($file);
            $lista = $this->lst();
            foreach ($lista as $ujfile) {
                $this->get_file_r($ujfile, $tmpadd . $file . "/");
            }
            ftp_cdup($this->ftp_handle);
        } else {
            @mkdir($this->tmpdir . $tmpadd, 0770, true);
            echo "Getting file: $file\n";
            $this->get_file($file, $this->tmpdir . $tmpadd . $file);
        }
    }

    function getTmpDir() {
        return $this->tmpdir;
    }

    function cleanup() {
        deltree($this->tmpdir);
    }

    function disconnect() {
        ftp_close($this->ftp_handle);
    }

}

class zip_util {

    protected $zipfile;
    protected $zip_handle;

    function zip_util($file) {
        $this->zip_handle = new ZipArchive();
        $this->zipfile = $file;
        if ($this->zip_handle->open($this->zipfile, ZIPARCHIVE::CREATE) !== TRUE) {
            die("Cannot open: " . $this->zipfile);
        }
    }

    function zip_file($root, $file) {
        $this->zip_handle->addFile($root . $file, $file);
    }

    function zip($root, $file) {
        if (is_dir($root . $file)) {
            $this->zip_handle->addEmptyDir($file);
            $lista = scandir($root . $file);
            foreach ($lista as $value) {
                if ($value != "." && $value != "..") {
                    $this->zip($root, $file . "/" . $value);
                }
            }
        } else {
            echo "Zipping file: $file\n";
            $this->zip_file($root, $file);
        }
    }

    function cleanup() {
        $this->zip_handle->close();
    }

}

class mail_util {

    protected $to;
    protected $subject;
    protected $from;
    protected $attachments;
    protected $headers;
    protected $message;
    protected $message_body;

    function mail_util($orig_to, $orig_subject, $orig_from, $orig_message) {
        $this->subject = "=?UTF-8?B?".base64_encode($orig_subject)."?=";
        $this->from = $orig_from;
        $this->to = array($orig_to);
        $this->attachments = array();
        $this->message_body = $orig_message;
    }

    function addTo($string) {
        $this->to = array_merge($this->to, array($string));
    }

    function addAttachment($string) {
        $this->attachments = array_merge($this->attachments, array($string));
    }

    function send() {
        $eol = "\n";
        $mime_boundary = "==Multipart_Boundary_x" . md5(time()) . "x";
        $htmlalt_mime_boundary = $mime_boundary . "_htmlalt";
        $this->headers.="From: " . $this->from . $eol;
        $this->headers.="Reply-To: " . $this->from . $eol;
        $this->headers.="Return-Path: " . $this->from . $eol;
        $this->headers.="Message-ID: <" . time() . "-" . $this->from . ">" . $eol;
        $this->headers.="X-Mailer: PHP v" . phpversion() . $eol;
        $this->headers.='MIME-Version: 1.0' . $eol;
        $this->headers.="Content-Type: multipart/mixed; boundary=\"{$mime_boundary}\"" . $eol;
        //$this->message.= "This is a multi-part message in MIME format.".$eol.$eol;
        $this->message.="--" . $mime_boundary . $eol;
        $this->message.= "Content-Type: text/plain; charset=\"UTF-8\"" . $eol;
        $this->message.= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
        $this->message.= $this->message_body . $eol . $eol;
        foreach ($this->attachments as $file) {
            echo "Attaching: $file\n";
            $fname_arr = explode("/", $file);
            $fname = $fname_arr[count($fname_arr) - 1];
            $this->message .= "--" . $mime_boundary . $eol;
            $this->message .= "Content-Type: application/octet-stream; name=\"" . $fname . "\"" . $eol;
            $this->message .= "Content-Transfer-Encoding: base64" . $eol;
            $this->message .= "Content-Description: " . $fname . $eol;
            $this->message .= "Content-Disposition: attachment; filename=\"" . $fname . "\"" . $eol . $eol;
            $this->message .= chunk_split(base64_encode(file_get_contents($file))) . $eol . $eol;
        }
        $this->message .= "--" . $mime_boundary . "--" . $eol . $eol;
        echo "Sending mail\n";
        foreach ($this->to as $to) {
            mail($to, $this->subject, $this->message, $this->headers);
        }
    }

}

class ftp_check {

    protected $ftp;
    protected $zip;
    protected $smail;
    protected $ftp_server;
    protected $ftp_user;
    protected $ftp_pass;
    protected $ftp_root;
    protected $mail_to;
    protected $mail_subject;
    protected $mail_from;
    protected $db;

    function ftp_check($db_file, $ftp_server, $ftp_user, $ftp_pass, $ftp_root, $mail_to, $mail_subject, $mail_from) {
        $this->ftp_server = $ftp_server;
        $this->ftp_user = $ftp_user;
        $this->ftp_pass = $ftp_pass;
        $this->ftp_root = $ftp_root;
        $this->mail_to = $mail_to;
        $this->mail_subject = $mail_subject;
        $this->mail_from = $mail_from;
        $this->db = $db_file;
    }

    function check() {
        $this->ftp = new ftp_util($this->ftp_server, $this->ftp_user, $this->ftp_pass, $this->ftp_root);

        $lista = $this->ftp->lst();
        if (!(@$db = file($this->db))) {
            $db = array("");
        }
        foreach ($lista as $item) {
            if (!in_array($item . "\n", $db)) {
                //add to db
                $file = fopen($this->db, "a");
                fwrite($file, "$item\n");
                fclose($file);
                //download
                $this->ftp->get_file_r($item, "");
                //zip only if directory
                $dir = $this->ftp->getTmpDir() . $item;
                if ($this->ftp->isdir($item)) {
                    $isdir = true;
                    $this->zip = new zip_util($dir . ".zip");
                    $this->zip->zip($this->ftp->getTmpDir(), $item);
                    $this->zip->cleanup();
                    //delete original
                    deltree($this->ftp->getTmpDir() . $item);
                }
                //create message
                $message = "$item";
                //mail
                //check file sizes
                $fileName = $dir;
                if ($isdir) {
                    $fileName = "$dir.zip";
                }
                if (filesize($fileName) > 7000000) {
                    //file bigger than 7 megs
                    //die("File too large!!!");
                    $ftoobig = true;
                    $message.="\nFile too big (above 7MiB) ! Please download manually!";
                } else {
                    $ftoobig = false;
                }

                $this->smail = new mail_util($this->mail_to, $this->mail_subject . $item, $this->mail_from, $message);
                if (!$ftoobig) {
                    $this->smail->AddAttachment($fileName);
                }
                $this->smail->send();
            }
        }
        $this->ftp->cleanup();
        $this->ftp->disconnect();
    }

}

$ftp_c = new ftp_check("db.txt", "serverip", "username", "password", "/directorytoscan", "address@where.to.send", "", "sender@address");
$ftp_c->check();

?>
